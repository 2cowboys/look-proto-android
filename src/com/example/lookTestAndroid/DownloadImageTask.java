package com.example.lookTestAndroid;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;

/**
 * @Author: ivan
 * Date: 15.06.14
 * Time: 18:11
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    ImageView imageView;
    TextView label;

    public DownloadImageTask(ImageView imageView, TextView label) {
        this.imageView = imageView;
        this.label = label;
    }

    @Override
    protected Bitmap doInBackground(String... urls) {
        String urlDisplay = urls[0];
        Bitmap bitmap = null;
        try {
            InputStream in = new URL(urlDisplay).openStream();
            bitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
//                Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (result != null) {
            imageView.setImageBitmap(result);
        }else{
            label.setText("No image returned");
        }
    }
}
