package com.example.lookTestAndroid;

import android.os.AsyncTask;
import android.util.Log;
import ch.boye.httpclientandroidlib.client.methods.CloseableHttpResponse;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.ContentType;
import ch.boye.httpclientandroidlib.entity.mime.MultipartEntityBuilder;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.entity.mime.content.StringBody;
import ch.boye.httpclientandroidlib.impl.client.CloseableHttpClient;
import ch.boye.httpclientandroidlib.impl.client.HttpClients;
import ch.boye.httpclientandroidlib.*;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @Author: ivan
 * Date: 15.06.14
 * Time: 20:15
 */
public class UploadImageTask extends AsyncTask<String, Void, String> {

    private File file;
    private String userName;

    public UploadImageTask(File path, String userName){
        this.file = path;
        this.userName = userName;
    }


    @Override
    protected String doInBackground(String... urls) {

        try {

            URL url = new URL(urls[0]);
            Log.d("LookTest", "URL = " + urls[0].toString());


            CloseableHttpClient httpClient = HttpClients.createDefault();
            try {
                HttpPost httppost = new HttpPost(url.toString());

                FileBody bin = new FileBody(file);
                StringBody userName = new StringBody(this.userName, ContentType.TEXT_PLAIN);


                HttpEntity reqEntity = MultipartEntityBuilder.create()
                        .addPart("username", userName)
                        .addPart("file", bin)
                        .build();


                httppost.setEntity(reqEntity);

                System.out.println("executing request " + httppost.getRequestLine());
                CloseableHttpResponse response = httpClient.execute(httppost);
                try {
                    System.out.println("----------------------------------------");
                    System.out.println(response.getStatusLine());
                    HttpEntity resEntity = response.getEntity();
                    if (resEntity != null) {
                        System.out.println("Response content length: " + resEntity.getContentLength());
                    }
                } finally {
                    response.close();
                }
            } finally {
                httpClient.close();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Done";
    }


    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Log.d("LookTest", "Upload complete");
    }
}
