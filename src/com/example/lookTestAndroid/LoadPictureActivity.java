package com.example.lookTestAndroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.inject.Inject;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.io.*;

@ContentView(R.layout.main)
public class LoadPictureActivity extends RoboActivity {

    static final int REQUEST_TAKE_PHOTO = 1;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String LOOK_TEST = "LookTest";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "0.1_alpha";
    public static final String PHOTO_PATH = "http://look-test.herokuapp.com/photo/";
    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    String SENDER_ID = "720950053126";

    private File pathToPhoto;
    private ApplicationSingelton appSettings;
    private GoogleCloudMessaging gcm;
    private String regid;
    private SharedPreferences prefs;
    private Context context;
    private String photoId;

    @InjectView(R.id.welcomeLabel) TextView welcomeLabel;
    @InjectView(R.id.imageView) ImageView imageView;
    @InjectView(R.id.tabHost) TabHost tabs;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);



        tabs.setup();

        TabHost.TabSpec spec = tabs.newTabSpec("tag1");

        spec.setContent(R.id.tab1);
        spec.setIndicator("Vote");
        tabs.addTab(spec);

        spec = tabs.newTabSpec("tag2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("My Looks");
        tabs.addTab(spec);

        tabs.setCurrentTab(0);

        context = getApplicationContext();

        // Check device for Play Services APK.
        if (checkPlayServices()) {

            appSettings = ((ApplicationSingelton) getApplicationContext());
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);

            if (regid.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(LOOK_TEST, "No valid Google Play Services APK found.");
        }

        // start Facebook Login
        Session.openActiveSession(this, true, new Session.StatusCallback() {

            // callback when session changes state
            @Override
            public void call(Session session, SessionState state, Exception exception) {
            if (session.isOpened()) {
                // make request to the /me API
                Request.newMeRequest(session, new Request.GraphUserCallback() {

                    // callback after Graph API response with user object
                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        if (user != null) {
                            welcomeLabel.setText("Hello " + user.getName() + "!" + user.getId());
                            appSettings.setUserName(user.getId());
                            sendRegistrationIdToBackend();
                        }
                    }
                }).executeAsync();
            }
            }
        });

        Intent intent = getIntent();
        Log.d(LOOK_TEST, "Intent get intent: " + intent);
        if (intent != null) {
            Log.d(LOOK_TEST, "Intent extra sting: " + intent.getStringExtra("Test"));
        }
    }

    public void onButtonVoteUpClick(View v) {
        if (photoId != null && !photoId.isEmpty()) {
            new SendVote().execute(PHOTO_PATH + photoId + "/like", appSettings.getUserName());
            loadNextPhoto();
        }
    }

    public void onButtonVoteDownClick(View v) {
        if (photoId != null && !photoId.isEmpty()) {
            new SendVote().execute(PHOTO_PATH + photoId + "/dislike", appSettings.getUserName());
            loadNextPhoto();
        }
    }

    /**
     * Load next photo from server
     */
    private void loadNextPhoto() {
        new DownloadImageTask(imageView, welcomeLabel)
                .execute(PHOTO_PATH + appSettings.getUserName() + "/next");
    }

    public void onLoadButtonClick(View v) throws IOException {

        photoId = "-5900850021255864893";
        // show The Image
        new DownloadImageTask(imageView, welcomeLabel)
                .execute(PHOTO_PATH + photoId);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                PhotoHandler photoHandler = new PhotoHandler();
                photoFile = photoHandler.createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.d(LOOK_TEST, ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                Log.d(LOOK_TEST, "Save file to " + Uri.fromFile(photoFile));
                pathToPhoto = photoFile;
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            Log.d(LOOK_TEST, "Get Photo");
            new DownloadImageTask(imageView, welcomeLabel)
                    .execute(Uri.fromFile(pathToPhoto).toString());
            Log.d(LOOK_TEST, "Upload Photo");
            new UploadImageTask(pathToPhoto, appSettings.getUserName()).execute(PHOTO_PATH);
        }else{
            super.onActivityResult(requestCode, resultCode, data);
            Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        }
    }

    public void onTakePictureClick(View view) {

        dispatchTakePictureIntent();
    }

    // You need to do the Play Services APK check here too.
    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();

        Intent intent = getIntent();
        Log.d(LOOK_TEST, "onResume Intent get intent: " + intent);
        if (intent != null) {
            Log.d(LOOK_TEST, "onResume Intent extra sting: " + intent.getStringExtra("Test") + ", " + intent.getAction());
        }
    }


    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(LOOK_TEST, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context context) {

        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(LOOK_TEST, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(LOOK_TEST, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(LoadPictureActivity.class.getSimpleName(),
        Context.MODE_PRIVATE);
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {

        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(LOOK_TEST, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend() {
        if (appSettings.getUserName() != null) {
            new SendRegIdToBackend().execute(appSettings.getUserName(), regid);
        }
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.d(LOOK_TEST, msg + "\n");
            }
        }.execute(null, null, null);
    }

}